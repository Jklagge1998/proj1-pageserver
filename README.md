# README #

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

### Project Overview ###
## Author: Jackson Klagge, Jklagge@uoregon.edu ##
Project Description: A basic webpage server that runs on a local host. 
What I did was I modified the pageserver.py file to give proper https responses to GET Requests. If the URL Request is valid (a html or css file in ./pages with no forbidden characters) the server will load up the contents of that file. If the URL request has forbidden characters it will give 403 Forbidden Status. If the URL request is not in ./pages then it will give 404 Not Found Status. I also copied the credentials-skel.ini file and modified it to credentials.ini and filled in the information required ( file is not in repository because it is a private file). I also replaced the sourcefile README.md with this one. The sourcefile README along with the source code for the project can be found at https://bitbucket.org/UOCIS322/proj1-pageserver.
